const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const multer = require('multer');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
const moment = require('moment');

const destPath = 'uploads/';
const upload = multer({ dest: destPath });
const port = process.env.PORT || 3000;
const privatekey = 'newsbackend'
const hostUrl = 'https://news--backend.herokuapp.com';

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const middleware = (req, res, next) => {
    
    let token = req.get("authToken");
    let payload = jwt.verify(token, privatekey);
    
    if(payload.user != undefined)
        next(); //อนุญาตให้ไปฟังก์ชันถัดไป
    else
        res.status(200).json({result : false , message: 'no authorization'});
}; 

var con = mysql.createConnection({
    host : '',
    port : '',
    user : '',
    password : '',
    database: ''
});

con.connect(function(err) {
    if(err) throw err;
});

app.get("/", function(req, res) {
    res.send("Backend is running");
});

app.get("/newlist",function(req, res) {
    
    con.query("SELECT id, title, description, image, DATE_FORMAT(created_date, '%m/%d/%Y %H:%i') as created_date from news ", function(err, rows, fields) {
        if(!err){
            
            let newList = rows.map( item => {
                if(item.description.length > 100 ){
                    item.description = item.description.substring(0, 100) + '...';
                }
                item.image = hostUrl + '/image/'+item.image;
                return item;
            })
            res.status(200).json({'result': true, 'newList' : rows});
        }
    })
});

app.post("/auth",function(req, res) {
    
    var username = req.body.username;
	var password = req.body.password;
    
    con.query('SELECT * from users where user = ? ' , [username ], function(err, rows, fields) {
        
        if(!err){
            if(rows.length == 1){
                let dbUser = rows[0].user;
                let dbPassword = rows[0].password;
                bcrypt.compare(password, dbPassword , function(err, result) {
                    var token = jwt.sign({ user : dbUser }, privatekey);
                    
                    if(result){
                        res.status(200).json({'result': true, 'token' : token});
                    }else{
                        res.status(200).json({'result': false, 'message' : 'login failed'});
                    }
                    
                });
            }else{
                res.status(200).json({'result': false, 'message' : 'no user'});
            }
        }
        
    })
    
    
});

app.post("/logout",function(req, res) {
    
    res.status(200).json({'result': true, 'message' : 'logout success'});
    
});

app.get("/new/:id",function(req, res) {
    var query = con.query('SELECT * from news WHERE id = ? ', req.params.id , function (error, rows, fields) {
        if (error) throw error;
        let newItem = rows[0];
        newItem.image = hostUrl + '/image/'+newItem.image;
        res.status(200).json({'result': true, 'new' : newItem});
    });
});

app.post("/new",middleware, upload.single('image'), function(req, res) {

    const tempPath = req.file.path;
    const targetPath = path.join(destPath , req.file.originalname);

    fs.rename(tempPath, targetPath, err => {
        if (err) return handleError(err, res);

        let newItem = { 
            title : req.body.title , 
            description : req.body.description ,
            image : req.file.originalname,
            created_date : moment().format('YYYY-MM-DD HH:mm:ss') ,
            updated_date : moment().format('YYYY-MM-DD HH:mm:ss')
        };

        con.query('INSERT INTO news SET ?', newItem , function ( error , results, fields) {
            
            res.status(200).json({'result': true, 'message' : 'Add new success'});
        });

    });
    
});

app.delete("/new/:id" , middleware,function(req, res) {
    
    var query = con.query('DELETE FROM news WHERE id = ? ', req.params.id , function (error, results, fields) {
        if (error) throw error;
    });

    res.status(200).json({'result': true, 'message' : 'delete success'});

});

app.patch("/new/:id", middleware, upload.single('image'),function(req, res) {


    const tempPath = req.file.path;
    const targetPath = path.join(destPath , req.file.originalname);

    fs.rename(tempPath, targetPath, err => {
        if (err) return handleError(err, res);

        var query = con.query('UPDATE news SET title = ? , description = ? , image =  ? , updated_date = ? WHERE id = ? ', 
            [ req.body.title , req.body.description , req.file.originalname , moment().format('YYYY-MM-DD HH:mm:ss') ,req.params.id] , function (error, results, fields) {
            if (error) throw error;
        });
    });
    res.status(200).json({'result': true, 'message' : 'update success'});

});

app.get("/image/:filename", upload.single('image'),function(req, res) {
    
    var options = {
        root: path.join(destPath)
    }

    res.sendFile(req.params.filename,options);
    
});

app.listen(port, function() {
    console.log('Our app is running on port : ' + port);
});
Before use is backend ,please run script for create table below this.

----- start script for create table for use this program -----

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

CREATE TABLE `users` (
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`user`, `password`) VALUES
('admin', '$2b$10$Nz9.NWsDInFXvN8UolkIreWpkaTbfnV7HRt/ifmY8bP/zVRuJiIfW');

ALTER TABLE `users`
  ADD UNIQUE KEY `user` (`user`);

----- end script -----

After run create table script , please set database config in server.js file at

var con = mysql.createConnection({
    host : '',  is ip or url of mysql database
    port : '',  is port of mysql database
    user : '',  is user of mysql database
    password : '',  is password of mysql database
    database: ''  is database of mysql database
});

